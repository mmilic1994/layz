class Dropdown {
    constructor({ dropdownParent }) {
        this.dropdownParent = dropdownParent;
        this.dropdownTrigger = this.dropdownParent.querySelector('.dropdown-trigger');
        this.dropdownMenu = this.dropdownParent.querySelector('.dropdown-menu');

        this.handleDropdownToggling = this.handleDropdownToggling.bind(this);
        this.handleDropdownOutsideClick = this.handleDropdownOutsideClick.bind(this);
        this.initEvents();
    }

    handleDropdownToggling() {
        const openedDropdown = document.querySelector('.dropdown.open');
        if(openedDropdown) {
            openedDropdown.classList.remove('open')
        }
        this.dropdownParent.classList.toggle('open');
    }

    handleDropdownOutsideClick(event) {
        const clickedElement = event.target;
        if (!clickedElement.closest('.dropdown.open')) {
            this.dropdownParent.classList.remove('open');
        }
    }

    closeAnyOpenDropdown() {
        const openedDropdown = document.querySelector('.dropdown.open');
        if (openedDropdown) {
            openedDropdown.classList.remove('open');
        }
    }


    initEvents() {
        this.dropdownTrigger.addEventListener('click', this.handleDropdownToggling);
        document.addEventListener('click', this.handleDropdownOutsideClick);
    }
}

const allDropdowns = document.querySelectorAll('.dropdown');
if (allDropdowns.length > 0) {
    allDropdowns.forEach((singleDropdown) => {
        const dropdownInstance = new Dropdown({
            dropdownParent: singleDropdown
        });
    })
}