let expand = document.querySelector('.expand');
let currencyPicker = document.querySelector('#currencyPicker');
let languagePicker = document.querySelector('#languagePicker');
let dropDownMenu = document.querySelector('.dropdown-menu');
let languageDropDown = document.querySelector('.language-dropdown');
let burger = document.querySelector('.icon-navicon-round');
let closeIcon = document.querySelector('.icon-close');
let mobileMenu = document.querySelector('.mobile-nav');
let thumbnails = document.querySelectorAll('.thumb');
let selectedProduct = document.querySelector('.selected-img');
let colorPicker = document.querySelectorAll('.sprite');
let colorPickerSelection = document.getElementsByClassName('color-picker-select');

// banner image expand

if (expand) {
    function handleExpand() {
        let bannerGreen = document.querySelector('.banner_stripe');
        bannerGreen.classList.toggle('open');
    }
    expand.addEventListener('click', handleExpand);
}

// mobile menus

if (burger) {
    function burgerMenuToggle() {
        mobileMenu.classList.toggle('open');
        burger.classList.add('close');
        closeIcon.classList.add('open');
    }
    burger.addEventListener('click', burgerMenuToggle);
}

if (closeIcon) {
    function closeIconMenuToggle() {
        mobileMenu.classList.toggle('open');
        burger.classList.remove('close');
        closeIcon.classList.remove('open');
    }
    closeIcon.addEventListener('click', closeIconMenuToggle);
}

// preview of a thumbnail

if (thumbnails) {
    for (let i = 0; i < thumbnails.length; i++) {
        thumbnails[i].addEventListener('click', selectedImage)
        thumbnails[i].addEventListener('click', fadeIn)
    }

    function selectedImage(event) {
        event.preventDefault();
        let imageSource = event.target.src;
        selectedProduct.src = imageSource;
    }

    function fadeIn() {
        if(!this.classList.contains('fadeIn')){
            selectedProduct.classList.add('fadeIn');
            setTimeout(()=> selectedProduct.classList.remove('fadeIn'), 500)
        }
    }
}

// display product based on the color picker selection

if(colorPicker) {

    function displayProducts(event) {
        event.preventDefault();
        let selectedColor = event.target.dataset.color;
        for(let i = 0; i < colorPickerSelection.length; i++) {
            const singleImage = colorPickerSelection[i];
            const imageColor = singleImage.dataset.color;
            if (selectedColor === imageColor) {
                singleImage.classList.remove('hidden');
                selectedProduct.src = singleImage.querySelector('img').src;
            } else {
                singleImage.classList.add('hidden');
            }
        }
    }

    for(let j = 0; j < colorPicker.length; j++) {
        colorPicker[j].addEventListener('click', displayProducts);
    }

}

new Drift(document.querySelector(".selected-img"), {
    paneContainer: document.querySelector(".magnify-modal"),
        inlinePane: 2560,
  });





