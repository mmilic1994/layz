// load modules
const gulp = require('gulp');
const del = require('del');
const livereload = require('gulp-livereload');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const concat = require('gulp-concat');

// delete all the assets in public
function assetsClean(done) {
    return del(
        ['public/**/*', '!public/css','!public/css/drift-basic.css', '!public/Drift.js'],
        { force: true }
    );
}

// publish HTML files
function htmlPublish(done) {
  return gulp.src('src/assets/*.html')
    .pipe(gulp.dest('public'))
    .pipe(livereload());
}

// compile SCSS files
function scssCompile(done) {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'ie 11', 'ie 10', 'Safari 8'],
      cascade: false
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('public/css'))
    .pipe(livereload());
}

// publish images
function imagesPublish() {
  return gulp.src('src/assets/img/*.{gif,jpg,jpeg,png}')
        .pipe(gulp.dest('public/img'))
}

// publish icons(svgs)
function iconsPublish() {
  return gulp.src('src/assets/icons/*.svg')
        .pipe(gulp.dest('public/icons'))
}

// compile custom scripts
function customScriptsCompile() {
  const scriptSources = [
    'src/assets/_dropdown.js',
    'src/assets/app.js',
  ];
  return gulp.src(scriptSources)
        .pipe(babel({
         presets: ['@babel/env']
        }))
        .pipe(concat('app.js'))
        .pipe(gulp.dest('public'))
}

// watch files
function watchFiles(done) {
    gulp.watch("src/assets/**/*.html", htmlPublish);
    gulp.watch("src/scss/**/*.scss", scssCompile);
    gulp.watch("src/assets/**/*.js", customScriptsCompile);
}

// start livereload
function livereloadStart(done) {
    livereload.listen();
}

// export tasks
exports.publish = gulp.series(assetsClean, htmlPublish, scssCompile, imagesPublish, iconsPublish, customScriptsCompile);
exports.watch = gulp.parallel(livereloadStart, watchFiles)